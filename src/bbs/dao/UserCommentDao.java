package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserComment;
import bbs.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.text as text, ");
            sql.append("users.name as name, ");
            sql.append("users.department_id as department_id, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());


            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String text = rs.getString("text");
                Timestamp created_date = rs.getTimestamp("created_date");
                int message_id = rs.getInt("message_id");
                String name = rs.getString("name");
                String department_id = rs.getString("department_id");
                String branch_id = rs.getString("branch_id");


                UserComment comment = new UserComment();
                comment.setId(id);
                comment.setUser_id(user_id);
                comment.setText(text);
                comment.setCreated_date(created_date);
                comment.setMessage_id(message_id);
                comment.setName(name);
                comment.setDepartment_id(department_id);
                comment.setBranch_id(branch_id);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
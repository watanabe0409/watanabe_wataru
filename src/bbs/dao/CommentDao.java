package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bbs.beans.Comment;
import bbs.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
//            sql.append("id");
            sql.append("user_id");
            sql.append(",text");
            sql.append(", created_date");
            sql.append(", message_id");
            sql.append(") VALUES (");
//            sql.append(" ?"); // id
            sql.append(" ?"); // user_id
            sql.append(", ?");// text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?");// message_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

 //           ps.setInt(1, message.getId());
            ps.setInt(1, comment.getUser_id());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getMessage_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, Integer DeleteId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments ");
            sql.append("WHERE ");
            sql.append("id =?;");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, DeleteId);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void andDelete(Connection connection, Integer CommentDeleteId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments ");
            sql.append("WHERE ");
            sql.append("message_id =?;");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, CommentDeleteId);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bbs.beans.Message;
import bbs.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
//            sql.append("id");
            sql.append("user_id");
            sql.append(",title");
            sql.append(",text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(") VALUES (");
//            sql.append(" ?"); // id
            sql.append(" ?"); // user_id
            sql.append(", ?");// title
            sql.append(", ?");// text
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

 //           ps.setInt(1, message.getId());
            ps.setInt(1, message.getUser_id());
            ps.setString(2, message.getTitle());
            ps.setString(3, message.getText());
            ps.setString(4, message.getCategory());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, Integer DeleteId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages ");
            sql.append("WHERE ");
            sql.append("id =?;");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, DeleteId);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_stopped");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // department_id
            sql.append(", 0"); // is_stopped
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getDepartment_id());
//            ps.setInt(6, user.getIs_stopped());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = loginUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }



//    public List<User> branchMerge(Connection connection) {
//
//        PreparedStatement ps = null;
//        try {
//            StringBuilder sql = new StringBuilder();
//            sql.append("SELECT ");
//            sql.append("branches.id as id, ");
//            sql.append("branches.name as name, ");
//            sql.append("users.branch_id as branch_id, ");
//            sql.append("FROM users ");
//            sql.append("INNER JOIN branches ");
//            sql.append("ON users.id = branches.id ");
//            sql.append("INNER JOIN depaertments ");
//            sql.append("ON users.id = departments.id ");
//
//            ps = connection.prepareStatement(sql.toString());
//
//            ResultSet rs = ps.executeQuery();
//            List<User> ret = getAllUser(rs);
//            return ret;
//        } catch (SQLException e) {
//            throw new SQLRuntimeException(e);
//        } finally {
//            close(ps);
//        }
//    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();


        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int is_stopped = rs.getInt("is_stopped");
                int department_id = rs.getInt("department_id");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setIs_stopped(is_stopped);
                user.setDepartment_id(department_id);


                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    private List<User> loginUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int is_stopped = rs.getInt("is_stopped");
                int department_id = rs.getInt("department_id");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setIs_stopped(is_stopped);
                user.setDepartment_id(department_id);


                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUserConfirmation(Connection connection, String loginId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            if(!StringUtils.isNullOrEmpty(user.getPassword()) && !"".equals(user.getPassword())){
            sql.append("UPDATE users SET");
            sql.append(" login_id = ? ");
            sql.append(", password = ? ");
            sql.append(", name = ? ");
            sql.append(", branch_id = ? ");
            sql.append(", department_id = ? ");
//            sql.append(", is_stopped");
            sql.append(" WHERE");
            sql.append(" id = ?");
            } else{

	            sql.append("UPDATE users SET");
	            sql.append(" login_id = ? ");
	            sql.append(", name = ? ");
	            sql.append(", branch_id = ? ");
	            sql.append(", department_id = ? ");
	//            sql.append(", is_stopped");
	            sql.append(" WHERE");
	            sql.append(" id = ?");
            }

            ps = connection.prepareStatement(sql.toString());

            if(!StringUtils.isNullOrEmpty(user.getPassword()) && !"".equals(user.getPassword())){
            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getDepartment_id());
//            ps.setInt(6,  user.getIs_stopped());
            ps.setInt(6, user.getId());
            } else {
            	ps.setString(1, user.getLogin_id());
	            ps.setString(2, user.getName());
	            ps.setInt(3, user.getBranch_id());
	            ps.setInt(4, user.getDepartment_id());
	//            ps.setInt(5,  user.getIs_stopped());
	            ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    public List<User> getAllUser(Connection connection) {

        PreparedStatement ps = null;
        try {

//            StringBuilder sql = new StringBuilder();
//            sql.append("SELECT ");
//            sql.append("branches.id as branch_id, ");
//            sql.append("branches.name as branch_name, ");
//            sql.append("departments.id as department_id, ");
//            sql.append("departments.name as department_name, ");
//            sql.append("users.branch_id as branch_id, ");
//            sql.append("users.department_id as department_id, ");
//            sql.append("users.id as id, ");
//            sql.append("users.login_id as login_id, ");
//            sql.append("users.password as password ");
//            sql.append("FROM users ");
//            sql.append("INNER JOIN branches ");
//            sql.append("ON users.id = branches.id ");
//            sql.append("INNER JOIN departments ");
//            sql.append("ON users.id = departments.id ");
//
//            ps = connection.prepareStatement(sql.toString());

            String sql = "SELECT * FROM users";

            ps = connection.prepareStatement(sql);
//            System.out.println(sql);
            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return null;
            } else {
                return userList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void stopped(Connection connection, List<Integer> StoppedId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users ");
            sql.append("SET is_stopped = ? ");
            sql.append("WHERE ");
            sql.append("id =?;");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, StoppedId.get(0));
            ps.setInt(2, StoppedId.get(1));

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User idCheck(Connection connection, String loginId) {

        PreparedStatement ps = null;
        try {
        	String sql = "SELECT * FROM users WHERE login_id = ?";

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, loginId);

            ResultSet rs = ps.executeQuery();
            List<User> checkResult = toUserList(rs);
            if (checkResult.isEmpty() == true) {
                return null;
            } else if (2 <= checkResult.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return checkResult.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
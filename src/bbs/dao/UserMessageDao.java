package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserMessage;
import bbs.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num, String searchCategory, String searchDate, String searchDateEnd) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.text as text, ");
            sql.append("messages.title as title, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("users.department_id as department_id, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE created_date BETWEEN ? ");
            sql.append("AND ? ");
            if(searchCategory != null){
            sql.append("AND category LIKE ? ");
            }
            sql.append("ORDER BY created_date DESC limit " + num);



            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, searchDate + " 00:00:00");
            ps.setString(2, searchDateEnd + " 23:59:59");
            if(searchCategory != null){
            ps.setString(3, "%" + searchCategory + "%");
            }
            System.out.println(ps.toString());
            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
              String title = rs.getString("title");
                String text = rs.getString("text");
              String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String name = rs.getString("name");
                String department_id = rs.getString("department_id");
                String branch_id = rs.getString("branch_id");

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setUser_id(user_id);
                message.setTitle(title);
                message.setText(text);
                message.setCategory(category);
                message.setCreated_date(createdDate);
                message.setName(name);
                message.setDepartment_id(department_id);
                message.setBranch_id(branch_id);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
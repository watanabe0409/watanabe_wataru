package bbs.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;


@WebFilter(urlPatterns = {"/management",  "/Edit"})
public class AuthorityFilter implements Filter{
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

        HttpSession session = ((HttpServletRequest)req).getSession();

        List<String> messages = new ArrayList<String>();

        User user = (User) session.getAttribute("loginUser");


        int department = user.getDepartment_id();


        if(department != 1){
            // 総務部でなければホームへ飛ぶ
        	messages.add("あなたには権限がありません");
            session.setAttribute("errorMessages", messages);
            ((HttpServletResponse)res).sendRedirect("./");
            return;
        }
        // 総務であればそのまま移行
        chain.doFilter(req, res);
    }

    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}
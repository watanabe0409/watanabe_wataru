package bbs.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;


@WebFilter("/*")
public class LoginFilter implements Filter{
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

        // セッションが存在しない場合NULLを返す
        HttpSession session = ((HttpServletRequest)req).getSession(false);

        List<String> messages = new ArrayList<String>();

        String path = ((HttpServletRequest)req).getServletPath();
        if(!"/login".equals(path) && !"/css/login".contains(path) && !"/css/style.css".contains(path)){
	        if(session == null ){

	            ((HttpServletResponse)res).sendRedirect("login");
	            return;
	        }

	        User user = (User) session.getAttribute("loginUser");

	        if(user == null){
	            // userがNullならば、ログイン画面へ飛ばす
	        	messages.add("ログインをしてください");
	            session.setAttribute("errorMessages", messages);
	            ((HttpServletResponse)res).sendRedirect("login");
	            return;
	        }

	        int is_stopped = user.getIs_stopped();
	        if(is_stopped == 1){
	        	messages.add("ログインに失敗しました。");
	            session.setAttribute("errorMessages", messages);
	            ((HttpServletResponse)res).sendRedirect("login");
	            return;
	        }
        }
        // セッションがNULLでなければ、通常どおりの遷移
        chain.doFilter(req, res);

    }

    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}
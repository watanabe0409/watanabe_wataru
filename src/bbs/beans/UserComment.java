package bbs.beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private String text;
    private Date created_date;
    private int user_id;
    private int message_id;
    private String department_id;
    private String branch_id;


    public int getId(){
    	return id;
    }

    public void setId(int id){
    	this.id = id;
    }

    public String getName(){
    	return name;
    }

    public void setName(String name){
    	this.name = name;
    }

    public String getText(){
    	return text;
    }

    public void setText(String text){
    	this.text = text;
    }

    public Date getCreated_date(){
    	return created_date;
    }

    public void setCreated_date(Date created_date){
    	this.created_date = created_date;
    }

    public int getUser_id(){
    	return user_id;
    }

    public void setUser_id(int user_id){
    	this.user_id = user_id;
    }

    public int getMessage_id(){
    	return message_id;
    }

    public void setMessage_id(int message_id){
    	this.message_id = message_id;
    }

    public String getDepartment_id(){
    	return department_id;
    }

    public void setDepartment_id(String department_id){
    	this.department_id = department_id;
    }

    public String getBranch_id(){
    	return branch_id;
    }

    public void setBranch_id(String branch_id){
    	this.branch_id = branch_id;
    }
}
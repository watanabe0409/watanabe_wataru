package bbs.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String text;
    private Date created_date;
    private int user_id;
    private int message_id;

    public int getId(){
    	return id;
    }

    public void setId(int id){
    	this.id = id;
    }

    public String getText(){
    	return text;
    }

    public void setText(String text){
    	this.text = text;
    }

    public Date getCreated_date(){
    	return created_date;
    }

    public void setCreated_date(Date created_date){
    	this.created_date = created_date;
    }

    public int getUser_id(){
    	return user_id;
    }

    public void setUser_id(int user_id){
    	this.user_id = user_id;
    }

    public int getMessage_id(){
    	return message_id;
    }

    public void setMessage_id(int message_id){
    	this.message_id = message_id;
    }
}
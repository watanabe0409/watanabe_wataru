package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.service.CommentService;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/MessageDelete" })
public class MessageDeteleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	int DeleteId = Integer.parseInt(request.getParameter("delete"));
    	int CommentDeleteId = Integer.parseInt(request.getParameter("delete"));

    	new MessageService().delete(DeleteId);
    	new CommentService().andDelete(CommentDeleteId);


    	request.setAttribute("DeleteId", DeleteId);
    	request.setAttribute("DeleteId", CommentDeleteId);
        response.sendRedirect("./");
    }
}

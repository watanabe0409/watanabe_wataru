package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.service.UserService;


@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	List<Branch> branches = new UserService().branchGetList();
        List<Department> departments = new UserService().departmentGetList();




        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	List<Branch> branches = new UserService().branchGetList();
        List<Department> departments = new UserService().departmentGetList();
        List<String> messages = new ArrayList<String>();
        User saveParameter = saveEdit(request);

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

            new UserService().register(user);



            response.sendRedirect("management");
        } else {
            request.setAttribute("branches", branches);
            request.setAttribute("departments", departments);
        	request.setAttribute("saveEdit", saveParameter);
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private User saveEdit(HttpServletRequest request)
            throws IOException, ServletException {

        User saveParameter = new User();

        saveParameter.setName(request.getParameter("name"));
        saveParameter.setLogin_id(request.getParameter("login_id"));
        saveParameter.setPassword(request.getParameter("password"));
        saveParameter.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        saveParameter.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
        return saveParameter;

    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String IdCheck = request.getParameter("login_id");

    	User checkResult = new UserService().idCheck(IdCheck);

    	String name = request.getParameter("name");
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        int branch_id = Integer.parseInt(request.getParameter("branch_id"));
        int department_id = Integer.parseInt(request.getParameter("department_id"));



        if(String.valueOf(name).length() <= 10 == false){
        	messages.add("名前は10文字以下で入力してください");
        }

        if(StringUtils.isEmpty(name) == true || StringUtils.isBlank(name) == true){
        	messages.add("名前を入力してください");
        }

        if (StringUtils.isEmpty(login_id) == true || StringUtils.isBlank(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (login_id.matches("^[a-zA-Z0-9]{6,20}$") == false && StringUtils.isBlank(login_id) == false) {
            messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");
        }
        if (checkResult != null) {
            messages.add("そのログインIDは存在しています");
        }
        if (StringUtils.isBlank(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (password.matches("^[/_\\-+@*a-zA-Z0-9]{6,20}$") == false && StringUtils.isEmpty(password) == false) {
            messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
        }
        if (password.matches(password2) == false) {
            messages.add("確認パスワードが間違ってます");
        }
        if (StringUtils.isEmpty(password2) == true) {
            messages.add("確認パスワードを入力してください");
        }
        if(department_id == 1){
	        if(branch_id + department_id != 2){
	        	messages.add("支店と役職の組み合わせが不正です");
	        }
        }
        if(department_id == 2){
	        if(branch_id + department_id != 3){
	        	messages.add("支店と役職の組み合わせが不正です");
	        }
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
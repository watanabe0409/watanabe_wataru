package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class UserManagement extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

         List<User> users = new UserService().bringUpUser(); //サービスの全ユーザ取得用のメソッド
         List<Branch> branches = new UserService().branchGetList();
         List<Department> departments = new UserService().departmentGetList();

         request.setAttribute("users", users);
         request.setAttribute("branches", branches);
         request.setAttribute("departments", departments);

         request.getRequestDispatcher("management.jsp").forward(request, response);

     }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	int MinStoppedId = Integer.parseInt(request.getParameter("MinstoppedId"));
    	int statusId = 0;

        if(MinStoppedId == 0){
       	 statusId = 1;
        }

        if(MinStoppedId == 1){
        	statusId = 0;
        }

        int userId = Integer.parseInt(request.getParameter("userId"));

        List<Integer> StoppedId = new ArrayList<Integer>();


        StoppedId.add(0, statusId);
        StoppedId.add(1, userId);

    	new UserService().stopped(StoppedId);

    	request.setAttribute("stoppedId", StoppedId);
    	response.sendRedirect("management");
//    	request.getRequestDispatcher("management.jsp").forward(request, response);
    }

}


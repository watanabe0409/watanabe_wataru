package bbs.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.User;
import bbs.beans.UserComment;
import bbs.beans.UserMessage;
import bbs.service.CommentService;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        String searchCategory = request.getParameter("searchCategory");
        String searchDate;
        String searchDateEnd;

        Calendar cl = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");


        if(request.getParameter("searchDate") == null || request.getParameter("searchDate").isEmpty() == true){
        	searchDate = "2017/5/25";
        } else {
        	searchDate = request.getParameter("searchDate");
        }



        if(request.getParameter("searchDateEnd") == null || request.getParameter("searchDateEnd").isEmpty() == true){
        	searchDateEnd = sdf.format(cl.getTime());

        } else {
        	searchDateEnd = request.getParameter("searchDateEnd");
        }

//        if(request.getParameter("searchCategory") == null || request.getParameter("searchCategory").isEmpty() == true){
//        	searchCategory = "*";
//        } else {
//        	searchCategory = request.getParameter("searchCategory");
//        }


        List<UserMessage> messages = new MessageService().getMessage(searchCategory, searchDate, searchDateEnd);
        List<UserComment> comments = new CommentService().getComment();

        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("searchCategory", searchCategory);
        request.setAttribute("searchDate", searchDate);
        request.setAttribute("searchDateEnd", searchDateEnd);
        request.setAttribute("isShowMessageForm", isShowMessageForm);


        request.getRequestDispatcher("top.jsp").forward(request, response);
    }

}
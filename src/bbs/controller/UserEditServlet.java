package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/Edit" })
public class UserEditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

    	String editId = request.getParameter("userEdit");
    	List<Branch> branches = new UserService().branchGetList();
        List<Department> departments = new UserService().departmentGetList();


//        if (isValidDoGet(request, messages) == true) {
//        	request.setAttribute("editUser", editUser);
//            request.setAttribute("branches", branches);
//            request.setAttribute("departments", departments);
//            request.getRequestDispatcher("Edit.jsp").forward(request, response);
//        } else {
//        	session.setAttribute("errorMessages", messages);
//        	response.sendRedirect("management");
//        }
        if (isValidDoGet(request, messages) != true) {
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("management");
        } else {

        User editUser = new UserService().getUser(Integer.parseInt(editId));

        	request.setAttribute("editUser", editUser);
            request.setAttribute("branches", branches);
            request.setAttribute("departments", departments);
            request.getRequestDispatcher("Edit.jsp").forward(request, response);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	List<Branch> branches = new UserService().branchGetList();
        List<Department> departments = new UserService().departmentGetList();

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            try {
            	new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.setAttribute("branches", branches);
                request.setAttribute("departments", departments);
                request.getRequestDispatcher("Edit.jsp").forward(request, response);
                return;
            }

            response.sendRedirect("management");
        } else {
            session.setAttribute("errorMessages", messages);
            if(request.getParameter("id") != null){
                User userName = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
                	request.setAttribute("userName", userName);
                }
            request.setAttribute("editUser", editUser);
            request.setAttribute("branches", branches);
            request.setAttribute("departments", departments);
            request.getRequestDispatcher("Edit.jsp").forward(request, response);
        }
    }



    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();

        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setName(request.getParameter("name"));
        editUser.setLogin_id(request.getParameter("login_id"));
        if(request.getParameter("password").matches("") == false){
        editUser.setPassword(request.getParameter("password"));
        }
        editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
        return editUser;

    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {



    	String idCheck = request.getParameter("login_id");
    	String editId = request.getParameter("id");
    	String password = request.getParameter("password");
    	String password2 = request.getParameter("password2");
    	String name = request.getParameter("name");
        String login_id = request.getParameter("login_id");
        int branch_id = Integer.parseInt(request.getParameter("branch_id"));
        int department_id = Integer.parseInt(request.getParameter("department_id"));

    	User checkResult = new UserService().idCheck(idCheck);
    	User editUser = new UserService().getUser(Integer.parseInt(editId));

        if(String.valueOf(name).length() <= 10 == false){
        	messages.add("名前は10文字以下で入力してください");
        }

        if(StringUtils.isEmpty(name) == true || StringUtils.isBlank(name) == true){
        	messages.add("名前を入力してください");
        }

        if (StringUtils.isEmpty(login_id) == true || StringUtils.isBlank(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (login_id.matches("^[a-zA-Z0-9]{6,20}$") == false && StringUtils.isBlank(login_id) == false) {
            messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");
        }
        if(editUser.getLogin_id().equals(login_id) == false){
	        if (checkResult != null) {
	            messages.add("そのログインIDは存在しています");
	        }
        }
        if(request.getParameter("password").matches("") == false){

	        if (password.matches("^[/_\\-+@*a-zA-Z0-9]{6,20}$") == false && StringUtils.isEmpty(login_id) == false) {
	            messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください");
	        }
	        if (password.matches(password2) == false) {
	            messages.add("確認パスワードが間違ってます");
	        }
	        if (StringUtils.isEmpty(password2) == true) {
	            messages.add("確認パスワードを入力してください");
        }

        }

        if(department_id == 1){
	        if(branch_id + department_id != 2){
	        	messages.add("支店と役職の組み合わせが不正です");
	        }
        }
        if(department_id == 2){
	        if(branch_id + department_id != 3){
	        	messages.add("支店と役職の組み合わせが不正です");
	        }
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidDoGet(HttpServletRequest request, List<String> messages) {

    	if(request.getParameter("userEdit") == null){
    		messages.add("不正パラメータが入力されました");
    		return false;
    	}

    	String userEditId = request.getParameter("userEdit");

    	if(userEditId.isEmpty() == true || userEditId.matches("^[0-9]*$") == false){
    		messages.add("不正パラメータが入力されました");
    		return false;
    	}

    	User existCheck = new UserService().getUser(Integer.parseInt(userEditId));

    	if(existCheck == null){
    		messages.add("不正パラメータが入力されました");
    	}

    	if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
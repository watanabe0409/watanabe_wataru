package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.service.CommentService;

@WebServlet(urlPatterns = { "/CommentDelete" })
public class CommentDeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	int DeleteId = Integer.parseInt(request.getParameter("delete"));

    	new CommentService().delete(DeleteId);

    	request.setAttribute("DeleteId", DeleteId);
        response.sendRedirect("./");
    }
}

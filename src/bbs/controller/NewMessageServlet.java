package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Message;
import bbs.beans.User;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("message.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        HttpSession session = request.getSession();

        String category = request.getParameter("category");
        String title = request.getParameter("title");
        String text = request.getParameter("text");

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setCategory(request.getParameter("category"));
            message.setTitle(request.getParameter("title"));
            message.setText(request.getParameter("text"));
            message.setUser_id(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("category", category);
            request.setAttribute("title", title);
            request.setAttribute("text", text);
            request.getRequestDispatcher("message.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String title = request.getParameter("title");
    	String category = request.getParameter("category");
        String message = request.getParameter("text");


        if(StringUtils.isBlank(title) == true) {
            messages.add("タイトルを入力してください");
        }
        if (30 < title.length()) {
            messages.add("タイトルは30文字以下で入力してください");
        }
        if(StringUtils.isBlank(category) == true) {
            messages.add("カテゴリを入力してください");
        }
        if (10 < category.length()) {
            messages.add("カテゴリは10文字以下で入力してください");
        }
        if (StringUtils.isBlank(message) == true) {
            messages.add("本文を入力してください");
        }
        if (1000 < message.length()) {
            messages.add("本文は1000文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
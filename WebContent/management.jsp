<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>

<script type="text/javascript">
<!--
	function disp() {

		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm('状態を変更してもよろしいですか？')) {

			// 「OK」時の処理終了
			return true;

			// 「キャンセル」時の処理開始
		} else {
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false;
		}
		// 「キャンセル」時の処理終了

	}
// -->
</script>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main-contents">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
			<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="page-title">ユーザー管理</div>

	<div class="header">
	<a href="signup" class="square_btn">ユーザー登録</a> <a href="./" class="square_btn">ホームへ戻る</a>
		<div class="management-margin" style="padding: 10px;">
		</div>
			<div class="contents" >
				<table border="5" style="margin: auto; ">
					<tr>
						<th>名前</th>
						<th>ログインID</th>
						<th>支店</th>
						<th>部署</th>
						<th>状態</th>
						<th>操作</th>
					</tr>
					<c:forEach items="${users}" var="user">

						<tr>
							<td><c:out value="${user.name}" /></td>
							<td><c:out value="${user.login_id}" /></td>
							<td> <c:forEach items="${branches}" var="branch">
										<c:if test="${branch.id == user.branch_id}">
											<c:out value="${branch.name}" />
										</c:if>
									</c:forEach></td>
							<td> <c:forEach items="${departments}" var="department">
										<c:if test="${department.id == user.department_id}">
											<c:out value="${department.name}" />
										</c:if>
								</c:forEach></td>
							<td>
								<c:if test="${user.is_stopped == 0 && user.id != loginUser.id}">
									現在の状態:活動
								</c:if>
								<c:if test="${user.is_stopped == 1 && user.id != loginUser.id}">
									現在の状態:<span style="color: #ff0000;">停止</span>
								</c:if>
								<c:if test="${loginUser.id == user.id}">
									現在の状態:ログイン中
								</c:if>
							</td>
							<td>
								<c:if test="${user.is_stopped == 0 && user.id != loginUser.id}">
										<form action="management" method="post">
											<input type="hidden" id="MinstoppedId" name="MinstoppedId" value="${user.is_stopped}">
											<input type="hidden" id="userId" name="userId" value="${user.id}">
											<input type="submit" value=" 停止させる " onClick="return disp();" style="background: #FE2E64;">
									</form>
								</c:if>
							<c:if test="${user.is_stopped == 1 && user.id != loginUser.id}">
										<form action="management" method="post">
										<input type="hidden" id="MinstoppedId" name="MinstoppedId"
												value="${user.is_stopped}"><input type="hidden"
												id="userId" name="userId" value="${user.id}"> <input
												type="submit" value=" 復活させる " onClick="return disp();">
										</form>
									</c:if>
									<c:if test="${loginUser.id == user.id}">
									ログイン中
									</c:if></td>

							<td><form action="Edit" method="get">
							<input type="hidden" id="userEdit" name="userEdit" value="${user.id}">
							<input type="submit" value=" 編集 " style="background: #9999CC;">
								</form></td>
						</tr>

					</c:forEach>
				</table>
			</div>
		</div>
		<div class="copyright"> Copyright(c)Wataru Watanabe</div>
	</div>
</body>
</html>
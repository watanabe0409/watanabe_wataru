<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:if test="${userName.name == null}">
	<title>${editUser.name}の設定</title>
</c:if>

<c:if test="${userName.name != null}">
	<title>${userName.name}の設定</title>
</c:if>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


<c:if test="${userName.name == null}">
	<div class="page-title">${editUser.name}の設定</div>
</c:if>

<c:if test="${userName.name != null}">
	<div class="page-title">${userName.name}の設定</div>
</c:if>


		<div class="header">
		<a href="management" class="square_btn">ユーザー管理へ戻る</a>
		<a href="./" class="square_btn">ホームへ戻る</a>
		</div>
	<div class="contents">
		<div class="contents-in">
			<form action="Edit" method="post">
				<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />
				<label for="name">名前<br /></label> <input name="name"
					value="${editUser.name}" id="name" /><br />

				<label for="login_id">ログインID<br /></label>
				<input name="login_id" value="${editUser.login_id}" style="ime-mode:disabled" /><br />
				<label for="password">パスワード<br /></label>
				<input name="password" type="password" id="password" /><br />
				<label for="password">パスワード確認<br /></label>
				<input name="password2" type="password" id="password" /> <br />


				 <c:if test="${editUser.id == loginUser.id}">
					<c:forEach items="${branches}" var="branch">
						<input type="hidden" id="branch_id" name="branch_id" value="${branch.id}">
					</c:forEach>
				</c:if>

					<c:if test="${editUser.id != loginUser.id}">
				支店名<br /><select name="branch_id">
						<c:forEach items="${branches}" var="branch">

							<c:if test="${branch.id == editUser.branch_id}">
								<option value="${branch.id}" id="branch_id" selected>${branch.name}</option></c:if>

							<c:if test="${branch.id != editUser.branch_id}">
								<option value="${branch.id}" id="branch_id" >${branch.name}</option></c:if>

						</c:forEach>
					</select>
					</c:if><br />

				<c:if test="${editUser.id == loginUser.id}">
					<c:forEach items="${departments}" var="department">
						<input type="hidden" id="department_id" name="department_id" value="${department.id}">
					</c:forEach>
				</c:if>

				 <c:if test="${editUser.id != loginUser.id}">部署・役職<br /> <select name="department_id">
					<c:forEach items="${departments}" var="department">
					<c:if test="${department.id == editUser.department_id}">
						<option value="${department.id}" id="department_id" selected>${department.name}</option></c:if>

					<c:if test="${department.id != editUser.department_id}">
						<option value="${department.id}" id="department_id" >${department.name}</option></c:if>
					</c:forEach>

				</select><br /> </c:if><br /> <input type="submit" value="変更" style="font-size: 25px; border-radius: 10px; margin: auto;" /> <br />
			</form>
			</div>
			<div class="copyright">Copyright(c)Wataru Watanabe</div>
		</div>
	</div>
</body>
</html>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
<!--
	function disp() {

		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm('投稿してもよろしいですか？')) {

			// 「OK」時の処理終了
			return true;

			// 「キャンセル」時の処理開始
		} else {
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false;
		}
		// 「キャンセル」時の処理終了

	}
// -->
</script>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
</head>
<body>
<div class="main-contents">
	<c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                    <ul>
	                        <c:forEach items="${errorMessages}" var="message">
	                            <li><c:out value="${message}" />
	                        </c:forEach>
	                    </ul>
	                </div>
	                <c:remove var="errorMessages" scope="session" />
	            </c:if>
<div class="page-title">新規投稿</div>

		<div class="header">
			<a href="./" class="square_btn">ホームへ戻る</a>
		</div>

		<div class="contents">
			<form action="newMessage" method="post">
				 <label for="title">タイトル<br /></label> <input name="title"
	                    id="title" value="${title}" /><br />
	                    <label for="category">カテゴリ<br /></label> <input name="category"
	                    id="category" value="${category}" />
				<br />本文<br />
				<textarea name="text" cols="100" rows="12" class="tweet-box" >${text}</textarea>
				<br /> <div class="selecter"><input type="submit" value="投稿" onClick="return disp();" style="font-size: 20px; border-radius: 10px; margin: auto;"></div>（1000文字まで）
			</form>
		</div>
		<div class="copyright">Copyright(c)Wataru Watanabe</div>
	</div>
</body>
</html>
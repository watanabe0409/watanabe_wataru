<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <div class="page-title">ユーザー登録</div>
			<div class="header">
			<a href="management" class="square_btn">ユーザー管理へ戻る</a>
			<a href="./" class="square_btn">ホームへ戻る</a>
			</div>

            <div class="contents">
            <form action="signup" method="post">
                <br /> <label for="name">名前<br /></label>
                <input name="name" id="name" value="${saveEdit.name }"/><br />
                <label for="login_id">ログインID<br /></label>
                <input name="login_id" id="login_id" value="${saveEdit.login_id }" style="ime-mode:disabled" /><br />
                <label for="password">パスワード<br /></label>
                <input name="password" type="password" id="password" /><br />
                <label for="password">パスワード確認<br /></label>
                <input name="password2" type="password" id="password" /><br />
				支店名<br />
				<select name="branch_id">
				<c:forEach items="${branches}" var="branch">
				<c:if test="${branch.id == saveEdit.branch_id}">
					<option value="${branch.id}" selected><c:out value="${branch.name}" /></option></c:if>

				<c:if test="${branch.id != saveEdit.branch_id}">
					<option value="${branch.id}" ><c:out value="${branch.name}" /></option></c:if>
				</c:forEach>
				</select>
	                    <br />部署・役職<br />

	 				<select name="department_id">
				<c:forEach items="${departments}" var="department">
				<c:if test="${department.id == saveEdit.department_id}">
					<option value="${department.id}" selected><c:out value="${department.name}" /></option></c:if>

				<c:if test="${department.id != saveEdit.department_id}">
					<option value="${department.id}" ><c:out value="${department.name}" /></option></c:if>
				</c:forEach>
				</select>
				<br />
	                <br /> <input type="submit" value="登録" style="font-size: 25px; border-radius: 10px; margin: auto;" /> <br />
            </form>
            </div>
            <div class="copyright">Copyright(c)Wataru Watanabe</div>
        </div>
    </body>
</html>
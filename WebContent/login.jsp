<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>ログイン</title>
    </head>
	    <body>
	        <div class="main-contents">

		            <c:if test="${ not empty errorMessages }">
		                <div class="errorMessages">
		                    <ul>
		                        <c:forEach items="${errorMessages}" var="message">
		                            <li><c:out value="${message}" />
		                        </c:forEach>
		                    </ul>
		                </div>
		                <c:remove var="errorMessages" scope="session"/>
		            </c:if>

		            <div class="page-title">連絡掲示板</div>

					<div class="contents">
			            <form action="login" method="post"><br />
			               <div class="selecter"><label for="login_id">ログインID</label>
			                <input name="loginId" id="loginId" value="${loginId}" style="ime-mode:disabled" /></div> <br />

			                <div class="selecter"><label for="password">パスワード</label>
			                <input name="password" type="password" id="password"/></div> <br />

			                <input type="submit" value="ログイン" style="margin: auto;" /> <br />
			            </form>
		        	</div>
		        <div class="copyright"> Copyright(c)Wataru Watanabe</div>
	        </div>
	    </body>
</html>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>連絡掲示板</title>

<script type="text/javascript">
<!--
	function disp() {

		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm('本当に削除しますか？')) {

			// 「OK」時の処理終了
			return true;

			// 「キャンセル」時の処理開始
		} else {
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false;
		}
		// 「キャンセル」時の処理終了

	}

	function commentdisp() {

		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm('コメントしてもよろしいですか？')) {

			// 「OK」時の処理終了
			return true;

			// 「キャンセル」時の処理開始
		} else {
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false;
		}
		// 「キャンセル」時の処理終了

	}
// -->
</script>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
	<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <div class="page-title">連絡掲示板</div>

		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
			<a href="newMessage" class="square_btn">新規投稿</a>
				<c:if test="${loginUser.department_id == 1}">
					<a href="management" class="square_btn">ユーザー管理</a>
				</c:if>
				<a href="logout" class="square_btn">ログアウト</a>
			</c:if>
		</div>
	<div class="search">
		<form action="./" method="get" style="padding: 20px; border-style: solid; border-color: #82ca9c; border-width: 0px 0px 0px 13px;">
			<label for="searchCategory">カテゴリ<br /></label>
			<input name="searchCategory" id="searchCategory" value="${searchCategory}" /><br />
			<br />日付<br />
			<input type="date" name="searchDate" id="searchDate" value="${searchDate}" >
			～
			<input type="date" name="searchDateEnd" id="searchDateEnd" value="${searchDateEnd}" ><br />
			<input type="submit" value="絞込み" /> <br />
        </form>
	</div>
	<div class="heading">投稿一覧</div>

	<c:forEach items="${messages}" var="message">

			<div class="message-text" style="box-shadow: 0 3px 4px rgba(0, 0, 0, 0.32); margin: 2em auto; padding: 15px;">


				<div class="message">

				<div class="date-name">
					名前:<c:out value="${message.name}" />

					<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
					<div class="texts">
						<div class="title">
							<span class="title1">タイトル:</span>
							<span class="title2"><c:out value="${message.title}" /></span>
						</div>
						<div class="message-text">
							<c:forEach var="s" items="${fn:split(message.text, '
')}">
								<c:out value="${s}" /><br />
							</c:forEach>
						</div>

						<c:if test="${message.user_id == loginUser.id || loginUser.department_id == 2 || (loginUser.department_id == 3 && loginUser.branch_id == message.branch_id)}">



							<form action="MessageDelete" method="post">
								<input type="hidden" id="delete" name="delete" value="${message.id}">
									<input type="submit" value=" 上記の内容を削除 " onClick="return disp();">

							</form>
						</c:if>
					</div>
					<span class="category">カテゴリ:<c:out value="${message.category}" /></span>
				</div>

			<div class="comments">
				<div class="comment-title">コメント</div>

				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.message_id == message.id}">
					<div class="comment">
						<div class="comment-padding">
							<span class="comment-date-name">
								名前:<c:out value="${comment.name}" />
								<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
							</span>
							<div class="comment-text">
								<c:forEach var="s" items="${fn:split(comment.text, '
')}">
							<c:out value="${s}" /><br />
							</c:forEach>
							</div>
							<c:if test="${comment.user_id == loginUser.id || loginUser.department_id == 2 || (loginUser.department_id == 3 && loginUser.branch_id == comment.branch_id)}">
								<form action="CommentDelete" method="post">
									<input type="hidden" id="delete" name="delete" value="${comment.id}">
									<div class="subtext"><input type="submit" value="上記の内容を削除" onClick="return disp();">
								</div>
								</form>

							</c:if>
							</div>
						</div>
					</c:if>

				</c:forEach>


				<c:if test="${commentSave.message_id != message.id}">
					<form action="Comment" method="post">
						<textarea name="text" cols="127" rows="8" class="tweet-box"></textarea>
						<br /> <input type="hidden" name="message_id"
							value="${message.id}">
						<fmt:formatDate value="${comment.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
						<input type="submit" value="コメント" onClick=" return commentdisp();">(500文字まで)
					</form>
				</c:if>

				<c:if test="${commentSave.message_id == message.id}">
					<form action="Comment" method="post">
						<textarea name="text" cols="127" rows="8" class="tweet-box">${commentSave.text}</textarea>
						<c:remove var="commentSave" scope="session" />
						<br /> <input type="hidden" name="message_id"
							value="${message.id}">
						<fmt:formatDate value="${comment.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
						<input type="submit" value="コメント" onClick=" return commentdisp();">(500文字まで)
					</form>
				</c:if>

				</div>
				</div>
			</c:forEach>
		<div class="copyright">Copyright(c)Wataru Watanabe</div>
	</div>
</body>
</html>